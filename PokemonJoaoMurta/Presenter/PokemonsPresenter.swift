//
//  PokemonPresenter.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 31/03/2022.
//

import Foundation
import Alamofire
import Kingfisher

protocol PokemonsViewDelegate: AnyObject {
    func dataSucceed()
    func dataFailed(error: Error)
    func pokemonDetailSucceed(pokemons: [PokemonViewModel]?)
    func pokemonDetailFailed(error: Error)
    func pokemonCharacteristicsSucceed(pokemon: PokemonViewModel?)
    func pokemonCharacteristicsFailed(error: Error)
}

class PokemonsPresenter {
    
    weak private var pokemonsViewDelegate : PokemonsViewDelegate?
    private var viewModel : PokemonsViewModel?
    private var indexPokemon = 1
    
    func setViewDelegate(pokemonsViewDelegate: PokemonsViewDelegate?){
        self.pokemonsViewDelegate = pokemonsViewDelegate
    }
    
    // MARK: - Pokemons
    func pokemons() {
        
        APIClient.pokemons { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
                
            case .success(let result):
                
                if let pokemons = result.results {
                    strongSelf.viewModel = PokemonsViewModel(pokemons: [PokemonViewModel(id: nil, name: nil, weight: nil, height: nil, imageURL: nil, description: nil, moves: nil)])
                    
                    for index in 0...pokemons.count-1 {
                        strongSelf.viewModel?.pokemons?.append(PokemonViewModel(id: index + 1, name: pokemons[index].name, weight: 0, height: 0, imageURL: "", description: "", moves: [""]))
                    }
                }
                
                let updateArray = strongSelf.viewModel?.pokemons?.filter { $0.id != nil}
                
                strongSelf.viewModel?.pokemons = updateArray
                
                strongSelf.pokemonsViewDelegate?.dataSucceed()
                print(result)
                
            case .failure(let error):
                strongSelf.pokemonsViewDelegate?.dataFailed(error: error)
                print(error)
                
            }
        }
    }
    
    // MARK: - PokemonDetail
    func pokemonDetail(indexPokemon: Int) {
        
        guard let pokemons = self.viewModel?.pokemons else { return }
        
        if self.indexPokemon < pokemons.count + 1 {
            
            APIClient.pokemonDetail(id: indexPokemon) { [weak self] result in
                guard let strongSelf = self else { return }
                switch result {
                    
                case .success(let result):
                    
                    
                    if strongSelf.indexPokemon == strongSelf.viewModel?.pokemons?[strongSelf.indexPokemon - 1].id {
                        strongSelf.viewModel?.pokemons?[strongSelf.indexPokemon - 1].weight = result.weight
                        strongSelf.viewModel?.pokemons?[strongSelf.indexPokemon - 1].height = result.height
                        strongSelf.viewModel?.pokemons?[strongSelf.indexPokemon - 1].imageURL = result.sprite.url
                        for index in 0...result.moves.count-1{
                            strongSelf.viewModel?.pokemons?[strongSelf.indexPokemon - 1].moves?.append(result.moves[index].move.name)
                            
                        }
                        let updateArray = strongSelf.viewModel?.pokemons?[strongSelf.indexPokemon - 1].moves?.filter { $0 != ""}
                        strongSelf.viewModel?.pokemons?[strongSelf.indexPokemon - 1].moves = updateArray
                    }
                    
                    strongSelf.indexPokemon = strongSelf.indexPokemon + 1
                    strongSelf.pokemonDetail(indexPokemon: strongSelf.indexPokemon)
                    
                    
                    print(result)
                    
                case .failure(let error):
                    strongSelf.pokemonsViewDelegate?.pokemonDetailFailed(error: error)
                    print(error)
                    
                }
            }
        } else {
            self.pokemonsViewDelegate?.pokemonDetailSucceed(pokemons: pokemons)
            print("UPDATE WITH DETAILS ---- \(pokemons)")
            self.indexPokemon = 1
        }
    }
    
    // MARK: - PokemonCharacteristics
    func pokemonCharacteristics(indexPokemon: Int) {
        
        APIClient.pokemonCharacteristics(id: indexPokemon) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
                
            case .success(let result):
                
                if indexPokemon == strongSelf.viewModel?.pokemons?[indexPokemon - 1].id {
                    
                    for index in 0...result.descriptions.count-1{
                        if result.descriptions[index].language.name == "en" {
                            strongSelf.viewModel?.pokemons?[indexPokemon - 1].description = result.descriptions[index].description
                        }
                    }
                }
                
                strongSelf.pokemonsViewDelegate?.pokemonCharacteristicsSucceed(pokemon: strongSelf.viewModel?.pokemons?[indexPokemon - 1])
                print(result)
                
            case .failure(let error):
                strongSelf.pokemonsViewDelegate?.pokemonCharacteristicsFailed(error: error)
                print(error)
                
            }
        }
    }
    
    // MARK: - Download and Cache Images
    //------->>>> // // I copied these two functions that are the same on the internet, by a programmer/youtuber "Rebeloper", I really like this code, very usable and very good for debugging, I understand 100% what happens
     
    func downloadImage(url: String, imageView: UIImageView) {
         
         guard let downloadURL = URL(string: url) else { return }
         let resource = ImageResource(downloadURL: downloadURL)
         let placeholder = UIImage(named: "pokedex")
         let processor = RoundCornerImageProcessor(cornerRadius: 20)
         
         imageView.kf.setImage(with: resource, placeholder: placeholder, options: [.processor(processor)], progressBlock: {
             (receivedSize, totalSize) in
             let percentage = (Float(receivedSize) / Float(totalSize)) * 100.0
             print("downloading progress \(percentage)‰")
         }) { (result) in
             self.handle(result)
         }
         
     }
     
     func handle(_ result: Result<RetrieveImageResult, KingfisherError>) {
         
         switch result {
         case .success(let imageResult):
             let image = imageResult.image
             let cacheType = imageResult.cacheType
             let source = imageResult.source
             let originalSource = imageResult.originalSource
             
             print("size: \(image.size), cache: \(cacheType), source: \(source), originalSource \(originalSource)")
             
             
         case .failure(let error):
             print(error)
         }
     }
}
