//
//  APIClient.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 27/03/2022.
//

import Alamofire
import UIKit

class APIClient {
    // MARK: - Generic Perform Request
    @discardableResult
    private static func performRequest<T:Decodable>(route:APIRouter, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<T, AFError>)->Void) -> DataRequest {
        return AF.request(route)
                        .responseDecodable (decoder: decoder){ (response: DataResponse<T, AFError>) in
                            completion(response.result)
        }
    }

    // MARK: - Pokemons  Request
    static func pokemons(completion:@escaping (Result<Pokemons, AFError>)->Void)  {
        performRequest(route: APIRouter.pokemons, completion: completion)
    }
    
    // MARK: - Pokemon Detail  Request
    static func pokemonDetail(id: Int, completion:@escaping (Result<PokemonDetails, AFError>)->Void)  {
        performRequest(route: APIRouter.pokemonDetail(id: id), completion: completion)
    }
    
    // MARK: - Pokemon Characteristics  Request
    static func pokemonCharacteristics(id: Int, completion:@escaping (Result<PokemonCharacteristics, AFError>)->Void)  {
        performRequest(route: APIRouter.pokemonCharacteristics(id: id), completion: completion)
    }
    
}

