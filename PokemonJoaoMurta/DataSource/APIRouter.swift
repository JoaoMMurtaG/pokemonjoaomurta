//
//  APIRouter.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 27/03/2022.
//

import Alamofire

enum APIRouter: URLRequestConvertible {
    
    case pokemons
    case pokemonDetail(id: Int)
    case pokemonCharacteristics(id: Int)
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .pokemons:
            return .get
        case .pokemonDetail:
            return .get
        case .pokemonCharacteristics:
            return .get
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .pokemons:
            return "pokemon"
        case .pokemonDetail(let id):
            return "pokemon/\(id)"
        case .pokemonCharacteristics(let id):
            return "characteristic/\(id)/"
        }
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .pokemons:
            return nil
        case .pokemonDetail:
            return nil
        case .pokemonCharacteristics:
            return nil
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        var url : URL
        
        url = try C.ConfigurationServer.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        
        // Parameters
        if let parameters = parameters {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
}

