//
//  Constants.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 27/03/2022.
//

import Foundation

// MARK: - Base URL
struct C {
    struct ConfigurationServer {
        static let baseURL = "https://pokeapi.co/api/v2/"
    }
    
    //Parameter Keys
    struct APIParameterKey {
        static let limit = "limit"
        static let offset = "offset"
    }
}

// MARK: - HTTPHeaderField
enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

// MARK: - ContentType
enum ContentType: String {
    case json = "application/json"
}
