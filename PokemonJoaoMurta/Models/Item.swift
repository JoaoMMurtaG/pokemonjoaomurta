//
//  Item.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 31/03/2022.
//

import Foundation

struct Item: Decodable {
    let name: String
    let url: String
}
