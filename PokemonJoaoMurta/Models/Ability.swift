//
//  Ability.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 31/03/2022.
//

import Foundation

struct Ability: Decodable {
    let ability: Item
}
