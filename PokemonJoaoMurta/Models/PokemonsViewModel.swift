//
//  PokemonsViewModel.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 01/04/2022.
//

import Foundation

class PokemonsViewModel {
    var pokemons: [PokemonViewModel]?

    init(pokemons: [PokemonViewModel]? = []) {
        self.pokemons = pokemons
    }
}


