//
//  PokemonViewModel.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 01/04/2022.
//

import Foundation

struct PokemonViewModel {
    var id: Int?
    var name: String?
    var weight: Int?
    var height: Int?
    var imageURL: String?
    var description: String?
    var moves: [String]?
    
    init(id: Int? = nil, name: String? = nil, weight: Int? = nil, height: Int? = nil,imageURL: String? = nil, description: String? = nil, moves: [String]? = nil ) {
        
        self.id = id
        self.name = name
        self.weight = weight
        self.height = height
        self.imageURL = imageURL
        self.description = description
        self.moves = moves
    }
}
