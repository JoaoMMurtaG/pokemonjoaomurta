//
//  Type.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 31/03/2022.
//

import Foundation

struct Type: Decodable {
    let type: Item
}
