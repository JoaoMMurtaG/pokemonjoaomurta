//
//  PokemonCharacteristics.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 01/04/2022.
//

import Foundation

struct PokemonCharacteristics: Decodable {
    let id: Int
    let geneModulo: Int
    let possibleValues: [Int]
    let highestStat: Item
    let descriptions: [Descriptions]
    
    private enum CodingKeys: String, CodingKey {
        case id
        case geneModulo = "gene_modulo"
        case possibleValues = "possible_values"
        case highestStat = "highest_stat"
        case descriptions
    }
}
