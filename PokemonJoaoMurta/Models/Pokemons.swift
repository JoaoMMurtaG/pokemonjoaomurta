//
//  Data.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 28/03/2022.
//

import Foundation

struct Pokemons: Decodable {
    let count : Int
    let next: String?
    let previous: String?
    let results: [Item]?
}
