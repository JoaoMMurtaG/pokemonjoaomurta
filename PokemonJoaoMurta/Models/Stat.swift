//
//  Stat.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 31/03/2022.
//

import Foundation

struct Stat: Decodable {
    let baseStat: Int
    let stat: Item
    
    private enum CodingKeys: String, CodingKey {
        case stat
        case baseStat = "base_stat"
    }
}
