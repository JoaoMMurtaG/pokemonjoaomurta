//
//  Descriptions.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 01/04/2022.
//

import Foundation

struct Descriptions: Decodable {
    let description: String
    let language: Item
}
