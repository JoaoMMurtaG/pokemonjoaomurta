//
//  Sprite.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 31/03/2022.
//

import Foundation

struct Sprite: Decodable {
    let url: String
    
    private enum CodingKeys: String, CodingKey {
        case url = "front_default"
    }
}
