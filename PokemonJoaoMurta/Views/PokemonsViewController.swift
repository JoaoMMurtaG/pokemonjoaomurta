//
//  ViewController.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 27/03/2022.
//

import UIKit

class PokemonsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    
    private let pokemonsPresenter = PokemonsPresenter()
    private var pokemons : [PokemonViewModel]?
    private var pokemonSelected : PokemonViewModel?
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pokemonsPresenter.setViewDelegate(pokemonsViewDelegate: self)
        pokemonsPresenter.pokemons()
        setupCollectionView()
    }
    
    // MARK: - setupCollectionView
    func setupCollectionView(){
        collectionView.register(PokemonCollectionViewCell.nib(), forCellWithReuseIdentifier: PokemonCollectionViewCell.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    // MARK: - prepareForSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
      if segue.identifier == "Detail" ,
         let viewController = segue.destination as? PokemonDetailViewController  {
          
          viewController.pokemon = pokemonSelected
      }
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
extension PokemonsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.pokemons?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PokemonCollectionViewCell.identifier, for: indexPath) as! PokemonCollectionViewCell
        
        if let pokemons = self.pokemons {
            cell.configure(with: pokemons[indexPath.row])
        }
        
        return cell
        
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        pokemonsPresenter.pokemonCharacteristics(indexPokemon: indexPath.row + 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        .cellSize(from: collectionView)
        
    }
}

// MARK: - PokemonsViewDelegate
extension PokemonsViewController: PokemonsViewDelegate {
    func pokemonCharacteristicsSucceed(pokemon: PokemonViewModel?) {
        print("SUCCEED POKEMON CHARACTERISTICS")
        self.pokemonSelected = pokemon
        performSegue(withIdentifier: "Detail", sender: nil)
        
    }
    
    func pokemonCharacteristicsFailed(error: Error) {
        print("FAILED POKEMON CHARACTERISTICS")
    }
    
    func pokemonDetailSucceed(pokemons: [PokemonViewModel]?) {
        print("SUCCEED POKEMON DETAIL")
        self.pokemons = pokemons
        self.collectionView.reloadData()
        
    }
    
    func pokemonDetailFailed(error: Error) {
        print("FAILED POKEMON DETAIL")
    }
    
    func dataSucceed() {
        pokemonsPresenter.pokemonDetail(indexPokemon: 1)
    }
    
    func dataFailed(error: Error) {
        print("FAILED POKEMONS")
    }
    
    
}

