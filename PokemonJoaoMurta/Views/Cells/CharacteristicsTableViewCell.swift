//
//  TableViewCell.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 02/04/2022.
//

import UIKit

class MovesTableViewCell: UITableViewCell {

    static let identifier = "MovesTableViewCell"
    
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    
    static func nib() -> UINib {
        return UINib(nibName: "MovesTableViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
       super.awakeFromNib()
        backgroundColor = .darkGray
        
        layer.cornerRadius = 20.0
        labelName.textColor = .white
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        pokemonImageView.image = nil
    }

}
