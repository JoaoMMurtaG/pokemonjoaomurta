//
//  PokemonCollectionViewCell.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 01/04/2022.
//

import UIKit
import Kingfisher
class PokemonCollectionViewCell: UICollectionViewCell {

    static let identifier = "PokemonCollectionViewCell"
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    
    private let pokemonsPresenter = PokemonsPresenter()
    
    // MARK: - nib
    static func nib() -> UINib {
        return UINib(nibName: "PokemonCollectionViewCell", bundle: nil)
    }
    
    // MARK: - awakeFromNib
    override func awakeFromNib() {
       super.awakeFromNib()
        backgroundColor = .darkGray
        
        layer.cornerRadius = 20.0
        labelName.textColor = .white
    }
    
    // MARK: - coder
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    // MARK: - awakeFromNib
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageView.image = nil
    }
    
    // MARK: - configure
    func configure(with pokemon: PokemonViewModel) {
        labelName.text = pokemon.name
        pokemonsPresenter.downloadImage(url: pokemon.imageURL ?? "", imageView: self.imageView)
    }
}
