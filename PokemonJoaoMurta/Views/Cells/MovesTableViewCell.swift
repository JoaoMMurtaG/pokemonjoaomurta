//
//  TableViewCell.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 02/04/2022.
//

import UIKit

class MovesTableViewCell: UITableViewCell {

    static let identifier = "MovesTableViewCell"
    
    @IBOutlet weak var moveName: UILabel!
    
    // MARK: - nib
    static func nib() -> UINib {
        return UINib(nibName: "MovesTableViewCell", bundle: nil)
    }
    
    // MARK: - awakeFromNib
    override func awakeFromNib() {
       super.awakeFromNib()
        backgroundColor = .darkGray
        
        moveName.textColor = .white
    }
    
    // MARK: - coder
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    // MARK: - configure
    func configure(with move: String) {
        moveName.text = move
    }

}
