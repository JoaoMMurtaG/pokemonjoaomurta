//
//  PokemonDetail.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 01/04/2022.
//

import Foundation
import UIKit


class PokemonDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    private let pokemonsPresenter = PokemonsPresenter()
    var pokemon : PokemonViewModel?
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupDetails()
        print("UPDATE ON DETAIL ---- \(self.pokemon)")
    }
    
    // MARK: - setupDetails
    func setupDetails() {
        nameLabel.text = "Name : " + (pokemon?.name ?? "")
        weightLabel.text = "Weight : \(pokemon?.weight ?? 0)"
        heightLabel.text = "Height : \(pokemon?.height ?? 0)"
        descriptionLabel.text = "Description : " + (pokemon?.description ?? "")
        nameLabel.textColor = .white
        weightLabel.textColor = .white
        heightLabel.textColor = .white
        descriptionLabel.textColor = .white
        pokemonsPresenter.downloadImage(url: pokemon?.imageURL ?? "", imageView: self.detailImageView)
        
    }
    
    // MARK: - setupTableView
    func setupTableView(){
        tableView.register(MovesTableViewCell.nib(), forCellReuseIdentifier: MovesTableViewCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
    }
}

// MARK: - UITableViewDataSource && UITableViewDelegate
extension PokemonDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovesTableViewCell.identifier, for: indexPath) as! MovesTableViewCell

        if let move = self.pokemon?.moves{

            cell.configure(with: move[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return  self.pokemon?.moves?.count ?? 0
    }
}
