//
//  UICollectionView.swift
//  PokemonJoaoMurta
//
//  Created by João Murta on 01/04/2022.
//

import UIKit

extension CGSize {
    static func cellSize(from collectionView: UICollectionView) -> CGSize {
        let size = (collectionView.bounds.width - 20) / 2
        return CGSize(width: size, height: size)
    }
}
