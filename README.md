# prozis-interview

### Author
João Murta

### Description
- Arquitetura de projeto utilizada: MVP(utilizei esta arquitetura não por ser melhor ou pior que as outras mas somente por opinião pessoal. Dessa forma o Presenter consegue manipular o Model e fazer update da View, assim sendo a View somente consegue chamar os métodos do Presenter e não manipulá-los diretamente. Toda a lógica fica responsável pelo presenter)
- Criei uma pequena camada de rede constituída por dois ficheiros(APIClient e APIRouter), em que no APIClient temos todos os requests necessários com um "performRequest()" genérico para os mesmos, em que de seguida é retornado callback para o presenter. APIRouter é um simples "roteador" de endpoints, headers e parameters necessários para os pedidos. Embora a API trabalhe com Urls inteiros nas respostas , tentei mesmo assim dividir o máximo que consegui.
- Ficheiro "Constants"( variáveis, structs e enums usados globalmente para os requests)

- GitFlow(em contexto normal criaria branches por cada feature realizada, neste caso só criei duas branches, "master" onde seria a branch principal para realeses ou algo do género, e "develop" onde desenvolvi tudo).

### Dependencies
  Alamofire(para a facilitação dos rquests às API's)
  Kingfisher( para o download e cache de imagens)


### P.S: 
Não percebi no pedido de ter no mínimo 6 descrições em cada Pokemon,  no enunciado do exercício, se era para fazer a call do endpoint "Characteristics" ou se eram os Moves na resposta do pedido "Pokemon", então resumidamente apresentei os dois. No "characteristics" , eu apresentei as descrições só em linguagem em inglesa "en". Mas fazendo com mais tempo meu,  utilizaria com o "Locale()" para apresentar de acordo com a linguagem do dispositivo.

 Estas semanas foram difíceis eu ter tempo disponível na empresa onde trabalho, sei que conseguiria fazer mais coisas , como por exemplo a paginação, mas foi mesmo o tempo que consegui fora do trabalho . Desde já agradeço a oportunidade. Cumprimentos.
